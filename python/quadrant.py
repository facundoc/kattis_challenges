#!/usr/bin/env python3
import sys


def __main__(x, y):
    """
        >>> __main__('1', '1')
        1

        >>> __main__('1', '-1')
        4

        >>> __main__('-1', '-1')
        3

        >>> __main__('-1', '1')
        2
    """

    if y.startswith('-'):
        if x.startswith('-'):
            q = 3
        else:
            q = 4
    else:
        if x.startswith('-'):
            q = 2
        else:
            q = 1

    return q


if __name__ == '__main__':
    data = sys.stdin.readlines()
    quadrant = __main__(*data)
    print(quadrant)
