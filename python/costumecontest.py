#!/usr/bin/env python3
"""https://open.kattis.com/problems/costumecontest
"""
import collections
import sys


def __main__(data):
    """
        >>> __main__(['ghost', 'mummy', 'witch', 'demon', 'demon', 'demon', 'demon', 'demon', 'demon', 'demon', ])
        ['ghost', 'mummy', 'witch']

        >>> __main__(['ghost', 'mysteryredwoman', 'ghost'])
        ['mysteryredwoman']
    """
    counts = collections.Counter(data)

    least_used = min(counts.values())

    return sorted([k for k, v in counts.items() if v == least_used])


if __name__ == '__main__':
    data = sys.stdin.readlines()
    categories = __main__(data[1:])
    print('\n'.join(categories))
