#!/usr/bin/env python3
"""https://open.kattis.com/problems/secretmessage
"""
import math
import sys


def __main__(message):
    """
        >>> __main__('abcd')
        'cadb'

        >>> __main__('iloveyoutooJill')
        'iteiloylloooJuv'

        >>> __main__('TheContestisOver')
        'OsoTvtnheiterseC'
    """

    message_len = len(message)
    row_len = math.ceil(math.sqrt(message_len))
    rows = []
    new_row = []
    for x in message:
        new_row.append(x)
        if row_len == len(new_row):
            rows.append(new_row)
            new_row = []
    if len(rows) < row_len:
        new_row += [''] * (row_len - len(new_row))
        rows.append(new_row)

    return ''.join((row[x] for x in range(row_len) for row in rows[::-1]))


if __name__ == '__main__':
    data = sys.stdin.readlines()
    for message in data[1:]:
        print(__main__(message.strip()))
