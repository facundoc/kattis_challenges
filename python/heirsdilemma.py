#!/usr/bin/env python3
"""https://open.kattis.com/problems/heirsdilemma
"""
import sys


def simple(l, h):
    n = 0

    for c in range(l, h + 1):

        if '0' in str(c):
            continue

        if len(set(str(c))) < 6:
            continue

        divisible = True
        for d in str(c):
            if d == '0':
                continue
            if c % int(d) != 0:
                divisible = False
                break

        if not divisible:
            continue

        n += 1

    return n


def compact(l, h):
    n = len([x for x in range(l, h + 1)
            if '0' not in str(x)
            and len(set(str(x))) == 6
            and all(x % int(d) == 0 for d in str(x))])

    return n


def __main__(l, h):
    """
        >>> __main__(123864, 123865)
        1

        >>> __main__(198765, 198769)
        0

        >>> __main__(200000, 300000)
        31
    """
    return compact(l, h)


if __name__ == '__main__':
    data = sys.stdin.read()
    l, h = (int(x) for x in data.split())
    combinations = __main__(l, h)
    print(combinations)
