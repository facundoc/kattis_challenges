#!/usr/bin/env python3
"""https://open.kattis.com/problems/yoda
"""
import itertools
import sys


def __main__(n, m):
    """
        >>> __main__('300', '500')
        '0\\n500'

        >>> __main__('65743', '9651')
        '673\\n95'

        >>> __main__('2341', '6785')
        'YODA\\n6785'
    """
    new_n = []
    new_m = []
    for nx, mx in itertools.zip_longest(n[::-1], m[::-1], fillvalue='0'):
        if mx <= nx:
            new_n.append(nx)

        if nx <= mx:
            new_m.append(mx)

    return '\n'.join((str(int(''.join(reversed(x)))) if x else 'YODA'
                      for x in (new_n, new_m)))


if __name__ == '__main__':
    data = sys.stdin.readlines()
    numbers = __main__(*(x.strip() for x in data))
    print(numbers)
