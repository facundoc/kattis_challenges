#!/usr/bin/env python3
"""https://open.kattis.com/problems/r2
"""
import sys


def __main__(r1, s):
    """
        >>> __main__('11', '15')
        19

        >>> __main__('4', '3')
        2
    """

    return int(s) * 2 - int(r1)


if __name__ == '__main__':
    data = sys.stdin.read()
    r2 = __main__(*data.split())
    print(r2)
