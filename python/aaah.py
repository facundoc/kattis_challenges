#!/usr/bin/env python3
"""https://open.kattis.com/problems/aaah
"""
import sys


def __main__(able_to_say, doctor_wants):
    """
        >>> __main__('aaah', 'aaaaah')
        'no'

        >>> __main__('aaah', 'ah')
        'go'
    """

    return 'go' if len(doctor_wants) <= len(able_to_say) else 'no'


if __name__ == '__main__':
    data = sys.stdin.readlines()
    answer = __main__(*data)
    print(answer)
